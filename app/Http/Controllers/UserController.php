<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Session;
if(!isset($_SESSION)) 
{ 
    session_start(); 
} 
class UserController extends Controller
{

	public function __construct(){
        $this->middleware('checkLogin');
    }
    
    public function showLoginPage(){
    	return view('login');
    }

    public function userLogin(Request $request){

    	$userName=$request->userName;
    	$password=md5($request->password);
    	
    	$result = DB::table('user')
    				->where('userName', $userName)
    				->where('password', $password)
    				->first();
    	if ($result) {
					Session::put('userId',$result->id);
					return redirect()->route('showRatingPage');
				}else{
					Session::put('message','User Name or Password Invalid');
					return redirect()->route('showLoginPage');
				}

    }

    
}
