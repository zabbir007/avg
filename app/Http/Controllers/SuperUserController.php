<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Session;
if(!isset($_SESSION)) 
{ 
    session_start(); 
} 
class SuperUserController extends Controller
{
	public function __construct(){
         
        $this->middleware('checkUser');
    }
    public function showRatingPage(){

    	$bookInfo=DB::table('book')
    				->where('status',1)
					->get();

    	return view('showBook',compact('bookInfo'));
    }

    public function userLogout(){
    	Session::put('userId','');
        return redirect()->route('showLoginPage');
    }

    public function rateBook(Request $request){

        $userId=Session::get('userId');
        $rateValue=$request->rateValue;  
        $bookId=$request->bookId; 
        $data=array();
        $data['bookRating']=$rateValue;
        $data['book_id']=$bookId;
        $data['user_id']=$userId;
        $checkRate=DB::table('book_rating')
                        ->where('user_id',$userId) 
                        ->where('book_id',$bookId) 
                        ->first();
        if ($checkRate) {
            
            $updateRate=DB::table('book_rating')
                            ->where('user_id',$userId) 
                            ->where('book_id',$bookId)
                            ->update($data);
            if ($updateRate) {
                echo json_encode('update');
                exit(); 
            }
        }else{

            $insertRate=DB::table('book_rating')
                            ->insert($data);
            if ($insertRate) {
                echo json_encode('success');
                exit(); 
            }
        }
    }
}
