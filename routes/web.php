<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::prefix('user')->group(function () {
Route::get('login', 'UserController@showLoginPage')->name('showLoginPage');
Route::post('signin', 'UserController@userLogin')->name('userLogin');
Route::get('rating-book', 'SuperUserController@showRatingPage')->name('showRatingPage');
Route::get('user-logout', 'SuperUserController@userLogout')->name('userLogout');
Route::post('rate-book', 'SuperUserController@rateBook')->name('rateBook');
});