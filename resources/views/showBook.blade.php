<!DOCTYPE html>
<html>
<head>
	<title>All Book</title>
	 <meta name="csrf-token" content="{{ csrf_token() }}">
     <meta name="_base_url" content="{{ url('/') }}">
	 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
	 <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
    
    <!-- Script -->
	  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
	  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
	  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
	  <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
</head>
<body>
	<a href="{{route('userLogout')}}"><button type="button">Logout</button></a>
	<br><br><br>
	<div class="container">
	  <h2>All Book</h2>
	              
	  <table class="table table-striped">
	    <thead>
	      <tr>
	        <th>Title</th>
	        <th>Price</th>
	        <th>Publisher Name</th>
	        <th>Author Name</th>
	        <th>Rate</th>
	        <th>Average Rate</th>
	      </tr>
	    </thead>
	    <tbody>
	    @foreach($bookInfo as $bookShow)
	    <?php
	    $userId=Session::get('userId');
	    
	    $avg_stars = DB::table('book_rating')
                ->where('book_id', $bookShow->id)
                ->avg('bookRating');
        if ($avg_stars=='') {
           
           	$avg_stars="Can't Rate Any User ";
        }
       
	    ?>
	      <tr>
	        <td>{{$bookShow->title}}</td>
	        <td>{{$bookShow->price}}</td>
	        <td>{{$bookShow->publisher}}</td>
	        <td>{{$bookShow->author}}</td>
	        <td>
	        	<input type="radio" data-id="{{$bookShow->id}}" name="rate" value="1">
	        	<input type="radio" data-id="{{$bookShow->id}}" name="rate" value="2">
	        	<input type="radio" data-id="{{$bookShow->id}}" name="rate" value="3">
	        	<input type="radio" data-id="{{$bookShow->id}}" name="rate" value="4">
	        	<input type="radio" data-id="{{$bookShow->id}}" name="rate" value="5">
	        </td>
	        <td>
	        	<?php echo $avg_stars;?>
	        </td>
	      </tr>
	      
	    @endforeach
	    </tbody>
	  </table>
	</div>
</body>
<script>
	$.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
      });
    $(document).ready(function(){
        $("input[type='radio']").click(function(){
            var radioValue = $("input[name='rate']:checked").val();
            var bookId = $(this).data("id");
            var APP_URL = $('meta[name="_base_url"]').attr('content');
            if(radioValue){
                
                Swal.fire(
				  'Sure You Rate This Book?',
				 
				)
                .then((willDelete) => {
                  if (willDelete) {

                         jQuery.ajax({
                            url: APP_URL+'/user/rate-book',
                            method: 'post',
                            data:{bookId:bookId,rateValue:radioValue},
                             beforeSend: function() {
                             
                            },
                            success: function(result){
                               var result = JSON.parse(result);
                               console.log(result);
                               if (result=="update") {
                                    //alert("jabbir");
                                    Swal.fire({
									  icon: 'error',
									  title: 'Oops...',
									  text: 'Your rating update successfully done',
									})
                                }else if(result=="success"){
                                       Swal.fire('successfully submit your rate')
                                }
                                
                            },
                              error: function() {
                                alert('Error occurs!');
                             }
                        });
                   
                  } 
                  
                });
            }
        });
    });
</script>
</html>